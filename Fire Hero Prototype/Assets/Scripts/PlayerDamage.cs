﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDamage : MonoBehaviour {

    public Camera Camera1;
    public Camera Camera3;
    public Camera Camera4;

    public AudioSource victoryMusic;
    public AudioSource mainTheme;

    public bool invincible = false;

    void OnCollisionEnter2D (Collision2D col)
    {
        if ((col.collider.CompareTag("Fire")) && !invincible) {
            this.gameObject.SetActive(false);
            Camera1.enabled = false;
            Camera3.enabled = true;
        }

        if (col.collider.CompareTag("Kitten"))
        {
            mainTheme.Stop();
            victoryMusic.Play();
            Camera1.enabled = false;
            Camera4.enabled = true;
        }
    }

	void Start () {
        Camera3.enabled = false;
        Camera1.enabled = true;
        Camera4.enabled = false;
        mainTheme.Play();
    }

    void Update () {
        if (Input.GetButtonDown("Submit"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        if (Input.GetButtonDown("Fire2"))
            invincible = !invincible;

    }
}
