﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTimer : MonoBehaviour {

    public float timeStart, timeRemaining;

    public GameObject player;
    public Slider timerBar;

    public Camera Camera1;
    public Camera Camera3;

    void Start () {
        timeStart = 90;
        timeRemaining = 90;
	}
	
	void Update () {

        timeRemaining -= Time.deltaTime;

        timerBar.value = (timeRemaining / timeStart);

        if ((timeRemaining == 0) || (timeRemaining < 0))
        {
            player.SetActive(false);
            Camera1.enabled = false;
            Camera3.enabled = true;
        }

        if (Input.GetButtonDown("Submit"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);


    }
}
