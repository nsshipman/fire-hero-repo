﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour {

    public Camera Camera1;
    public Camera Camera2;

    // Use this for initialization
    void Start () {

        

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "Door")
        {
            Camera1.gameObject.SetActive(false);
            Camera2.gameObject.SetActive(true);
        }
    }
}
