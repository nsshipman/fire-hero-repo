﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    [HideInInspector]
    public bool facingRight = true;        
    [HideInInspector]
    public bool jump = false;

    public float moveForce = 365f;          
    public float maxSpeed = 5f;                    
    public float jumpForce = 1000f;         
      
    private bool grounded = false;          
    private Animator anim;                

    private int jumpsRemaining;

    public Camera Camera1;
    public Camera Camera2;

    public AudioSource jumpSFX;
    public AudioSource axeHit;
    public AudioSource axeWhiff;
    

    void Awake()
    {
        anim = GetComponent<Animator>();
        Camera1.enabled = false;
		Camera2.enabled = true;
    }


    void Update()
    {
        if (Input.GetButtonDown("Jump") && (jumpsRemaining > 0))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));
            jumpsRemaining --;
            jumpSFX.Play();
        }

        int layer_mask = LayerMask.GetMask("Box");

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 10, layer_mask);

        if (facingRight)
            hit = Physics2D.Raycast(transform.position, Vector2.right, 10, layer_mask);
        else
            hit = Physics2D.Raycast(transform.position, Vector2.left, 10, layer_mask);

        if (Input.GetButtonDown("Fire1"))
        {
            if (hit.collider.CompareTag("Crate"))
            {
                print("hit");
                Destroy(hit.transform.gameObject);
                axeHit.Play();
            }
            //else axeWhiff.Play();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("ground") || col.collider.CompareTag("Crate"))
        {
            jumpsRemaining = 2;
        }

        if (col.gameObject.name == "Door")
        {
            transform.position = new Vector2(50, 0);
            Camera1.enabled = false;
            Camera2.enabled = true;
        }
    }



    void FixedUpdate ()
	{
		float h = Input.GetAxis("Horizontal");

        if (h > 0)
            facingRight = true;
        else if (h < 0)
            facingRight = false; 

		if(h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
			GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

		if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
			GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

		}
	}
